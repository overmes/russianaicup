import model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.StrictMath.PI;

public class Physics {
    public static Game game;

//    public Action[][] getAllAvailableActions(WorldState world) {
//        State[] teammates = world.teammates;
//        Action[][] result = new Action[teammates.length][];
//        for(int i = 0; i < teammates.length; i++){
//            result[i] = getAvailableActions(world, teammates[i], tickDiff);
//        }
//        return result;
//    }

    public Action[] getAvailableActions(WorldState world, State self) {
        List<Action> result = new ArrayList<>();
        if(self.knockdownTicks <= 0) {
            if(self.state != HockeyistState.SWINGING){
                Action[] moveActions = getMovementActions(world, self);

                Action[] forceActions = new Action[0];
                if (self.cooldown <= 0){
                    forceActions = getForceActions(world, self);
                }

                if (forceActions.length > 0) {
                    result = new ArrayList<Action>(Arrays.asList(zipActions(moveActions, forceActions)));
                } else {
                    result = new ArrayList<Action>(Arrays.asList(moveActions));
                }
                Action[] hitTurnActions = getHitTurnActions(world, self);
                if (hitTurnActions != null) result.addAll(new ArrayList<Action>(Arrays.asList(hitTurnActions)));
            } else {
                Action hitAction = getHitAction(self, world.puck, ActionType.STRIKE);
                if(hitAction != null){
                    result.add(hitAction);
                }
                result.add(new Action(self, ActionType.CANCEL_STRIKE));
            }

            if (self.swingTicks < 20){
                Action swingAction = getHitAction(self, world.puck, ActionType.SWING);
                if(swingAction != null) result.add(swingAction);
            }

        } else {
            result = new ArrayList<>();
        }

        return result.toArray(new Action[0]);
    }

    private Action[] appendActions(Action[] result, Action[] hitTurnActions) {
        if (hitTurnActions != null && hitTurnActions.length > 0){
            Action[] new_res = new Action[result.length+hitTurnActions.length];
            for(int i = 0; i < result.length+hitTurnActions.length; i++){
                new_res[i] = i < result.length ? result[i]:hitTurnActions[i - result.length];
            }
            return new_res;
        }
        return result;

    }

    public Action[] zipActions(Action[] moveActions, Action[] forceActions) {
        Action[] result = new Action[moveActions.length*forceActions.length];
        for (int ma = 0; ma < moveActions.length; ma++) {
            for (int fa = 0; fa < forceActions.length; fa++) {
                result[ma*forceActions.length + fa] = new Action(moveActions[ma], forceActions[fa]);
            }
        }
        return result;
    }

    public Action[] getForceActions(WorldState worldState, State self) {
        List<Action> actionList = new ArrayList<>();

        if (self.state != HockeyistState.SWINGING){
            PuckState puck = worldState.puck;
            if ((puck.owner == null || !puck.owner.parent.isTeammate() || puck.probability < 0.8) && isInStickSector(self, puck)){
                actionList.add(new Action(self, ActionType.TAKE_PUCK));
            }

            Action passAction = getPassHitAction(self, puck);
            if(passAction != null){
                actionList.add(passAction);
            }

            Action hitAction = getHitAction(self, puck, ActionType.STRIKE);
            if(hitAction != null){
                actionList.add(hitAction);
            }
        }


        return actionList.toArray(new Action[0]);
    }

    public Action getHitAction(State self, PuckState puck, ActionType type) {
        Action result = null;
        if (puck.owner != null && puck.owner.getId() == self.getId()) {
            State target = Utility.enemyBottomCorner;
            if(self.y > 400) target = Utility.enemyTopCorner;

            double targetAngle = getViewAngle(target, puck);
            double hitAngle = getViewAngle(self, target);
            if( hitAngle < 0.1 && hitAngle > -0.1){
                if (Utility.enemyLeft){
                    double maxAngle = 1.26;
                    if(self.x < 500 && self.x > 200 && ((targetAngle < maxAngle && targetAngle > -maxAngle) || MyStrategy.overtime)) result = new Action(self, type);
                } else {
                    double maxAngle = 1.06 + 3.14/2;
                    if(self.x > 700 && self.x < 1000 && ((targetAngle < -maxAngle || targetAngle > maxAngle) || MyStrategy.overtime)) result = new Action(self, type);
                }
            }
        }
        return result;
    }

    public static boolean isInStickSector(State self, PuckState puck){
        return getDistance(self, puck) <= game.getStickLength() && Math.abs(getViewAngle(self, puck)) <= game.getStickSector();
    }

    public Action getPassHitAction(State self, PuckState puck){
        Action result = null;
        if (puck.owner != null && puck.owner.getId() == self.getId()) {
            State target = Utility.enemyBottomCorner;
            if(self.y > 400) target = Utility.enemyTopCorner;

            double targetAngle = getViewAngle(target, puck);
            double passAngle = getViewAngle(self, target);
            if(passAngle < Math.PI/3 && passAngle > -Math.PI/3){
                //default 1.16
                if (Utility.enemyLeft){
                    double maxAngle = 0.96;
                    //уменьшать для более острой атаки
                    if(self.x < 500 && self.x > 200 && ((targetAngle < maxAngle && targetAngle > -maxAngle) || MyStrategy.overtime)) result = new Action(self, ActionType.PASS, passAngle, 1.0);
                } else {
                    //увеличивать для более острой атаки
                    double maxAngle = 1.36 + 3.14/2;
                    if(self.x > 700 && self.x < 1000 && ((targetAngle > maxAngle || targetAngle < maxAngle) || MyStrategy.overtime)) result = new Action(self, ActionType.PASS, passAngle, 1.0);
                }
            }
        }
        return result;
    }

    private Action[] getMovementActions(WorldState world, State self) {
        double[] availableSpeedUp = {-1, 0, 1};
        double turnFactor = WorldState.game.getHockeyistTurnAngleFactor();
        double[] availableTurn = {turnFactor, 0, -turnFactor};

        int movementsCount = availableSpeedUp.length * availableTurn.length;
        Action[] result = new Action[movementsCount];

        int i = 0;
        for (double currentSpeedUp: availableSpeedUp) {
            for (double currentTurn: availableTurn) {
                result[i] = new Action(self, currentSpeedUp, currentTurn);
                i++;
            }
        }
        return result;
    }

    public Action[] getHitTurnActions(WorldState world, State self) {
        PuckState puck = world.puck;
        Action[] result = null;
        if (puck.owner != null && puck.owner.getId() == self.getId()){
            State target = Utility.enemyBottomCorner;
            if(self.y > 400) target = Utility.enemyTopCorner;

            double hitAngle = getViewAngle(self, target)/Variables.moveCooldown;
            double targetAngle = getViewAngle(target, puck);
            if(hitAngle > 0.02 && hitAngle < game.getHockeyistTurnAngleFactor() && hitAngle > -game.getHockeyistTurnAngleFactor()){
                if (Utility.enemyLeft){
                    double maxAngle = 1.16;
                    if(self.x < 500 && self.x > 200 && ((targetAngle < maxAngle && targetAngle > -maxAngle) || MyStrategy.overtime)) result = getHitTurnActonsMass(self, hitAngle);
                } else {
                    double maxAngle = 1.16 + 3.14/2;
                    if(self.x > 700 && self.x < 1000 && ((targetAngle < -maxAngle || targetAngle > maxAngle) || MyStrategy.overtime)) result = getHitTurnActonsMass(self, hitAngle);
                }
            }

        }
        return result;
    }

    public Action[] getHitTurnActonsMass(State self, double angle){
        double[] availableSpeedUp = {-1, 0, 1};
        Action[] result = new Action[availableSpeedUp.length];
        for(int i=0; i < availableSpeedUp.length; i++){
            result[i] = new Action(self, availableSpeedUp[i], angle);
        }
        return result;
    }

    public static double normalizeX(double x, double radius) {
        double maxX = game.getRinkRight() - radius;
        if (x > maxX) x = maxX;
        double minX = game.getRinkLeft() + radius;
        if (x < minX) x = minX;
        return x;
    }

    public static double normalizeY(double y, double radius) {
        double maxY = game.getRinkBottom() - radius;
        if (y > maxY) y = maxY;
        double minY = game.getRinkTop() + radius;
        if (y < minY) y = minY;
        return y;
    }

    public static double getViewAngle(State state, PuckState puck) {
        double absoluteAngleTo = getAngle(state, puck);
        return getRelativeAngle(absoluteAngleTo, state.angle);
    }

    public static double getViewAngle(State state, State other) {
        double absoluteAngleTo = getAngle(state, other);
        return getRelativeAngle(absoluteAngleTo, state.angle);
    }

    public static double getRelativeAngle(double absoluteAngleTo, double angle){
        double relativeAngleTo = absoluteAngleTo - angle;

        while (relativeAngleTo > PI) {
            relativeAngleTo -= 2.0D * PI;
        }

        while (relativeAngleTo < -PI) {
            relativeAngleTo += 2.0D * PI;
        }

        return relativeAngleTo;
    }

    public static double getAngle(double x1, double y1, double x2, double y2) {
        return Math.atan2(y2 - y1, x2 - x1);
    }

    public static double getAngle(State stateOne, State stateTwo) {
        return getAngle(stateOne.x, stateOne.y, stateTwo.x, stateTwo.y);
    }

    public static double getAngle(State stateOne, PuckState puck) {
        return getAngle(stateOne.x, stateOne.y, puck.x, puck.y);
    }

    public static double getDistance(double x1, double y1, double x2, double y2) {
        return Math.hypot(x1 - x2, y1 - y2);
    }

    public static double getDistance(State stateOne, State stateTwo) {
        return getDistance(stateOne.x, stateOne.y, stateTwo.x, stateTwo.y);
    }

    public static double getDistance(State stateOne, PuckState puck) {
        return getDistance(stateOne.x, stateOne.y, puck.x, puck.y);
    }

    public static boolean isOvertime(World world){
        if(game.getTickCount() > world.getTick()){
            return true;
        }
        return false;
    }

}
