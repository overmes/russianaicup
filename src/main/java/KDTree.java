import java.io.Serializable;
import java.util.List;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;

/**
* KDTree is a class supporting KD-tree insertion, deletion, equality
* search, range search, and nearest neighbor(s) using double-precision
* floating-point keys.  Splitting dimension is chosen naively, by
* depth modulo K.  Semantics are as follows:
*
* <UL>
* <LI> Two different keys containing identical numbers should retrieve the 
*      same value from a given KD-tree.  Therefore keys are cloned when a 
*      node is inserted.
* <BR><BR>
* <LI> As with Hashtables, values inserted into a KD-tree are <I>not</I>
*      cloned.  Modifying a value between insertion and retrieval will
*      therefore modify the value stored in the tree.
*</UL>
*
* Implements the Nearest Neighbor algorithm (Table 6.4) of
*
* <PRE>
* &*064;techreport{AndrewMooreNearestNeighbor,
    *   author  = {Andrew Moore},
    *   title   = {An introductory tutorial on kd-trees},
    *   institution = {Robotics Institute, Carnegie Mellon University},
    *   year    = {1991},
    *   number  = {Technical Report No. 209, Computer Laboratory, 
    *              University of Cambridge},
    *   address = {Pittsburgh, PA}
* }
* </PRE>
*  
* Copyright (C) Simon D. Levy and Bjoern Heckel 2014
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as 
* published by the Free Software Foundation, either version 3 of the 
* License, or (at your option) any later version.
*
* This code is distributed in the hope that it will be useful,     
* but WITHOUT ANY WARRANTY without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License 
*  along with this code.  If not, see <http:*www.gnu.org/licenses/>.
*  You should also have received a copy of the Parrot Parrot AR.Drone 
*  Development License and Parrot AR.Drone copyright notice and disclaimer 
*  and If not, see 
*   <https:*projects.ardrone.org/attachments/277/ParrotLicense.txt> 
* and
*   <https:*projects.ardrone.org/attachments/278/ParrotCopyrightAndDisclaimer.txt>.
*/
public class KDTree<T> implements Serializable{
    // number of milliseconds
    final long m_timeout;
    
    // K = number of dimensions
    final private int m_K;
    
    // root of KD-tree
    private KDNode<T> m_root;
    
    // count of nodes
    private int m_count;
    
    /**
    * Creates a KD-tree with specified number of dimensions.
    *
    * @param k number of dimensions
    */
  
    public KDTree(int k) {
        this(k, 0);
    }
    public KDTree(int k, long timeout) {
        this.m_timeout = timeout;
	m_K = k;
	m_root = null;
    }
    
    
    /** 
    * Insert a node in a KD-tree.  Uses algorithm translated from 352.ins.c of
    *
    *   <PRE>
    *   &*064;Book{GonnetBaezaYates1991,                                   
	*     author =    {G.H. Gonnet and R. Baeza-Yates},
	*     title =     {Handbook of Algorithms and Data Structures},
	*     publisher = {Addison-Wesley},
	*     year =      {1991}
    *   }
    *   </PRE>
    *
    * @param key key for KD-tree node
    * @param value value at that key
    *
    * @throws KeySizeException if key.length mismatches K
    * @throws KeyDuplicateException if key already in tree
    */
    public void insert(double [] key, T value) 
    throws KeySizeException, KeyDuplicateException {
        this.edit(key, new Editor.Inserter<T>(value));
    }
    
    /** 
    * Edit a node in a KD-tree
    *
    * @param key key for KD-tree node
    * @param editor object to edit the value at that key
    *
    * @throws KeySizeException if key.length mismatches K
    * @throws KeyDuplicateException if key already in tree
    */
    
    public void edit(double [] key, Editor<T> editor) 
    throws KeySizeException, KeyDuplicateException {
	
	if (key.length != m_K) {
	    throw new KeySizeException();
	}
	
        synchronized (this) {
            // the first insert has to be synchronized
            if (null == m_root) {
                m_root = KDNode.create(new HPoint(key), editor);
                m_count = m_root.deleted ? 0 : 1;
                return;
            }
	}
	
        m_count += KDNode.edit(new HPoint(key), editor, m_root, 0, m_K);
    }
    
    /** 
    * Find  KD-tree node whose key is identical to key.  Uses algorithm 
    * translated from 352.srch.c of Gonnet & Baeza-Yates.
    *
    * @param key key for KD-tree node
    *
    * @return object at key, or null if not found
    *
    * @throws KeySizeException if key.length mismatches K
    */
    public T search(double [] key) throws KeySizeException {
	
	if (key.length != m_K) {
	    throw new KeySizeException();
	}
	
	KDNode<T> kd = KDNode.srch(new HPoint(key), m_root, m_K);
	
	return (kd == null ? null : kd.v);
    }
    
    
    public void delete(double [] key) 
    throws KeySizeException, KeyMissingException {
        delete(key, false);
    }
    /** 
    * Delete a node from a KD-tree.  Instead of actually deleting node and
    * rebuilding tree, marks node as deleted.  Hence, it is up to the caller
    * to rebuild the tree as needed for efficiency.
    *
    * @param key key for KD-tree node
    * @param optional  if false and node not found, throw an exception
    *
    * @throws KeySizeException if key.length mismatches K
    * @throws KeyMissingException if no node in tree has key
    */
    public void delete(double [] key, boolean optional) 
    throws KeySizeException, KeyMissingException {
	
	if (key.length != m_K) {
	    throw new KeySizeException();
	}
        KDNode<T> t = KDNode.srch(new HPoint(key), m_root, m_K);
        if (t == null) {
            if (optional == false) {
                throw new KeyMissingException();
            }
        }
        else {
            if (KDNode.del(t)) {
                m_count--;
            }
        }
    }
    
    /**
    * Find KD-tree node whose key is nearest neighbor to
    * key. 
    *
    * @param key key for KD-tree node
    *
    * @return object at node nearest to key, or null on failure
    *
    * @throws KeySizeException if key.length mismatches K
    
    */
    public T nearest(double [] key) throws KeySizeException {
	
	List<T> nbrs = nearest(key, 1, null);
	return nbrs.get(0);
    }
    
    /**
    * Find KD-tree nodes whose keys are <i>n</i> nearest neighbors to
    * key. 
    *
    * @param key key for KD-tree node
    * @param n number of nodes to return
    *
    * @return objects at nodes nearest to key, or null on failure
    *
    * @throws KeySizeException if key.length mismatches K
    
    */
    public List<T> nearest(double [] key, int n) 
    throws KeySizeException, IllegalArgumentException {
        return nearest(key, n, null);
    }
    
    /**
    * Find KD-tree nodes whose keys are within a given Euclidean distance of
    * a given key.
    *
    * @param key key for KD-tree node
    * @param d Euclidean distance
    *
    * @return objects at nodes with distance of key, or null on failure
    *
    * @throws KeySizeException if key.length mismatches K
    
    */
    public List<T> nearestEuclidean(double [] key, double dist) 
    throws KeySizeException {
	return nearestDistance(key, dist, new EuclideanDistance());
    }
    
    
    /**
    * Find KD-tree nodes whose keys are within a given Hamming distance of
    * a given key.
    *
    * @param key key for KD-tree node
    * @param d Hamming distance
    *
    * @return objects at nodes with distance of key, or null on failure
    *
    * @throws KeySizeException if key.length mismatches K
    
    */
    public List<T> nearestHamming(double [] key, double dist) 
    throws KeySizeException {
	
	return nearestDistance(key, dist, new HammingDistance());
   }
    
    
    /**
    * Find KD-tree nodes whose keys are <I>n</I> nearest neighbors to
    * key. Uses algorithm above.  Neighbors are returned in ascending
    * order of distance to key. 
    *
    * @param key key for KD-tree node
    * @param n how many neighbors to find
    * @param checker an optional object to filter matches
    *
    * @return objects at node nearest to key, or null on failure
    *
    * @throws KeySizeException if key.length mismatches K
    * @throws IllegalArgumentException if <I>n</I> is negative or
    * exceeds tree size 
    */
    public List<T> nearest(double [] key, int n, Checker<T> checker) 
    throws KeySizeException, IllegalArgumentException {
	
	if (n <= 0) {
            return new LinkedList<T>();
	}
	
	NearestNeighborList<KDNode<T>> nnl = getnbrs(key, n, checker);
	
        n = nnl.getSize();
        Stack<T> nbrs = new Stack<T>();
        
	for (int i=0; i<n; ++i) {
	    KDNode<T> kd = nnl.removeHighest();
            nbrs.push(kd.v);
	}
	
	return nbrs;
    }
    
    
    /** 
    * Range search in a KD-tree.  Uses algorithm translated from
    * 352.range.c of Gonnet & Baeza-Yates.
    *
    * @param lowk lower-bounds for key
    * @param uppk upper-bounds for key
    *
    * @return array of Objects whose keys fall in range [lowk,uppk]
    *
    * @throws KeySizeException on mismatch among lowk.length, uppk.length, or K
    */
    public List<T> range(double [] lowk, double [] uppk) 
    throws KeySizeException {
	
	if (lowk.length != uppk.length) {
	    throw new KeySizeException();
	}
	
	else if (lowk.length != m_K) {
	    throw new KeySizeException();
	}
	
	else {
	    List<KDNode<T>> found = new LinkedList<KDNode<T>>();
	    KDNode.rsearch(new HPoint(lowk), new HPoint(uppk), 
	    m_root, 0, m_K, found);
            List<T> o = new LinkedList<T>();
            for (KDNode<T> node : found) {
                o.add(node.v);
	    }
	    return o;
	}
    }
    
    public int size() { /* added by MSL */
        return m_count;
    }
    
    public String toString() {
	return m_root.toString(0);
    }
    
    private NearestNeighborList<KDNode<T>> getnbrs(double [] key) 
    throws KeySizeException {
	return getnbrs(key, m_count, null);
    }
    
    
    private NearestNeighborList<KDNode<T>> getnbrs(double [] key, int n, 
    Checker<T> checker) throws KeySizeException {
	
	if (key.length != m_K) {
	    throw new KeySizeException();
	}
	
	NearestNeighborList<KDNode<T>> nnl = new NearestNeighborList<KDNode<T>>(n);
	
	// initial call is with infinite hyper-rectangle and max distance
	HRect hr = HRect.infiniteHRect(key.length);
	double max_dist_sqd = Double.MAX_VALUE;
	HPoint keyp = new HPoint(key);
	
        if (m_count > 0) {
            long timeout = (this.m_timeout > 0) ? 
	    (System.currentTimeMillis() + this.m_timeout) : 
	    0;
            KDNode.nnbr(m_root, keyp, hr, max_dist_sqd, 0, m_K, nnl, checker, timeout);
        }
	
	return nnl;
	
    }
    
    private  List<T> nearestDistance(double [] key, double dist, 
    DistanceMetric metric) throws KeySizeException {
	
	NearestNeighborList<KDNode<T>> nnl = getnbrs(key);	    
	int n = nnl.getSize();
	Stack<T> nbrs = new Stack<T>();
	
	for (int i=0; i<n; ++i) {
	    KDNode<T> kd = nnl.removeHighest();
	    HPoint p = kd.k;
	    if (metric.distance(kd.k.coord, key) < dist) {
		nbrs.push(kd.v);
	    }
	}
	
	return nbrs;
    }


    abstract static class DistanceMetric {

        protected abstract double distance(double [] a, double [] b);
    }

    /**
    * KDTree.KeyDuplicateException is thrown when the <TT>KDTree.insert</TT> method
    * is invoked on a key already in the KDTree.
    *
    *
    * Copyright (C) Simon D. Levy 2014
    *
    * This code is free software: you can redistribute it and/or modify
    * it under the terms of the GNU Lesser General Public License as
    * published by the Free Software Foundation, either version 3 of the
    * License, or (at your option) any later version.
    *
    * This code is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    *  You should have received a copy of the GNU Lesser General Public License
    *  along with this code.  If not, see <http:*www.gnu.org/licenses/>.
    *  You should also have received a copy of the Parrot Parrot AR.Drone
    *  Development License and Parrot AR.Drone copyright notice and disclaimer
    *  and If not, see
    *   <https:*projects.ardrone.org/attachments/277/ParrotLicense.txt>
    * and
    *   <https:*projects.ardrone.org/attachments/278/ParrotCopyrightAndDisclaimer.txt>.
    */
    public static class KeyDuplicateException extends KDException {

        protected KeyDuplicateException() {
            super("Key already in tree");
        }

        // arbitrary; every serializable class has to have one of these
        public static final long serialVersionUID = 1L;
    }

    static class KDNode<T> implements Serializable{

        // these are seen by KDTree
        protected HPoint k;
        T v;
        protected KDNode<T> left, right;
        protected boolean deleted;

        // Method ins translated from 352.ins.c of Gonnet & Baeza-Yates
        protected static <T> int edit(HPoint key, Editor<T> editor, KDNode<T> t, int lev, int K)
         throws KeyDuplicateException {
            KDNode<T> next_node = null;
            int next_lev = (lev+1) % K;
        synchronized (t) {
                if (key.equals(t.k)) {
                    boolean was_deleted = t.deleted;
                    t.v = editor.edit(t.deleted ? null : t.v );
                    t.deleted = (t.v == null);

                    if (t.deleted == was_deleted) {
                        // if I was and still am deleted or was and still am alive
                        return 0;
                    } else if (was_deleted) {
                        // if I was deleted => I am now undeleted
                        return 1;
                    }
                    // I was not deleted, but I am now deleted
                    return -1;
                } else if (key.coord[lev] > t.k.coord[lev]) {
                    next_node = t.right;
                    if (next_node == null) {
                        t.right = create(key, editor);
                        return t.right.deleted ? 0 : 1;
                    }
                }
                else {
                    next_node = t.left;
                    if (next_node == null) {
                        t.left = create(key, editor);
                        return t.left.deleted ? 0 : 1;
                    }
                }
        }

            return edit(key, editor, next_node, next_lev, K);
        }

        protected static <T> KDNode<T> create(HPoint key, Editor<T> editor)
            throws KeyDuplicateException {
            KDNode<T> t = new KDNode<T>(key, editor.edit(null));
            if (t.v == null) {
                t.deleted = true;
            }
            return t;
        }

        protected static <T> boolean del(KDNode<T> t) {
            synchronized (t) {
                if (!t.deleted) {
                    t.deleted = true;
                    return true;
                }
            }
            return false;
        }

        // Method srch translated from 352.srch.c of Gonnet & Baeza-Yates
        protected static <T> KDNode<T> srch(HPoint key, KDNode<T> t, int K) {

        for (int lev=0; t!=null; lev=(lev+1)%K) {

            if (!t.deleted && key.equals(t.k)) {
            return t;
            }
            else if (key.coord[lev] > t.k.coord[lev]) {
            t = t.right;
            }
            else {
            t = t.left;
            }
        }

        return null;
        }

        // Method rsearch translated from 352.range.c of Gonnet & Baeza-Yates
        protected static <T> void rsearch(HPoint lowk, HPoint uppk, KDNode<T> t, int lev,
                      int K, List<KDNode<T>> v) {

        if (t == null) return;
        if (lowk.coord[lev] <= t.k.coord[lev]) {
            rsearch(lowk, uppk, t.left, (lev+1)%K, K, v);
        }
            if (!t.deleted) {
                int j = 0;
                while (j<K && lowk.coord[j]<=t.k.coord[j] &&
                       uppk.coord[j]>=t.k.coord[j]) {
                    j++;
                }
                if (j==K) v.add(t);
            }
        if (uppk.coord[lev] > t.k.coord[lev]) {
            rsearch(lowk, uppk, t.right, (lev+1)%K, K, v);
        }
        }

        // Method Nearest Neighbor from Andrew Moore's thesis. Numbered
        // comments are direct quotes from there.   KDTree.NearestNeighborList solution
        // courtesy of Bjoern Heckel.
       protected static <T> void nnbr(KDNode<T> kd, HPoint target, HRect hr,
                                  double max_dist_sqd, int lev, int K,
                                  NearestNeighborList<KDNode<T>> nnl,
                                  Checker<T> checker,
                                  long timeout) {

           // 1. if kd is empty then set dist-sqd to infinity and exit.
           if (kd == null) {
               return;
           }

           if ((timeout > 0) && (timeout < System.currentTimeMillis())) {
               return;
           }
           // 2. s := split field of kd
           int s = lev % K;

           // 3. pivot := dom-elt field of kd
           HPoint pivot = kd.k;
           double pivot_to_target = HPoint.sqrdist(pivot, target);

           // 4. Cut hr into to sub-hyperrectangles left-hr and right-hr.
           //    The cut plane is through pivot and perpendicular to the s
           //    dimension.
           HRect left_hr = hr; // optimize by not cloning
           HRect right_hr = (HRect) hr.clone();
           left_hr.max.coord[s] = pivot.coord[s];
           right_hr.min.coord[s] = pivot.coord[s];

           // 5. target-in-left := target_s <= pivot_s
           boolean target_in_left = target.coord[s] < pivot.coord[s];

           KDNode<T> nearer_kd;
           HRect nearer_hr;
           KDNode<T> further_kd;
           HRect further_hr;

           // 6. if target-in-left then
           //    6.1. nearer-kd := left field of kd and nearer-hr := left-hr
           //    6.2. further-kd := right field of kd and further-hr := right-hr
           if (target_in_left) {
               nearer_kd = kd.left;
               nearer_hr = left_hr;
               further_kd = kd.right;
               further_hr = right_hr;
           }
           //
           // 7. if not target-in-left then
           //    7.1. nearer-kd := right field of kd and nearer-hr := right-hr
           //    7.2. further-kd := left field of kd and further-hr := left-hr
           else {
               nearer_kd = kd.right;
               nearer_hr = right_hr;
               further_kd = kd.left;
               further_hr = left_hr;
           }

           // 8. Recursively call Nearest Neighbor with paramters
           //    (nearer-kd, target, nearer-hr, max-dist-sqd), storing the
           //    results in nearest and dist-sqd
           nnbr(nearer_kd, target, nearer_hr, max_dist_sqd, lev + 1, K, nnl, checker, timeout);

           KDNode<T> nearest = nnl.getHighest();
           double dist_sqd;

           if (!nnl.isCapacityReached()) {
               dist_sqd = Double.MAX_VALUE;
           }
           else {
               dist_sqd = nnl.getMaxPriority();
           }

           // 9. max-dist-sqd := minimum of max-dist-sqd and dist-sqd
           max_dist_sqd = Math.min(max_dist_sqd, dist_sqd);

           // 10. A nearer point could only lie in further-kd if there were some
           //     part of further-hr within distance max-dist-sqd of
           //     target.
           HPoint closest = further_hr.closest(target);
           if (HPoint.sqrdist(closest, target) < max_dist_sqd) {

               // 10.1 if (pivot-target)^2 < dist-sqd then
               if (pivot_to_target < dist_sqd) {

                   // 10.1.1 nearest := (pivot, range-elt field of kd)
                   nearest = kd;

                   // 10.1.2 dist-sqd = (pivot-target)^2
                   dist_sqd = pivot_to_target;

                   // add to nnl
                   if (!kd.deleted && ((checker == null) || checker.usable(kd.v))) {
                       nnl.insert(kd, dist_sqd);
                   }

                   // 10.1.3 max-dist-sqd = dist-sqd
                   // max_dist_sqd = dist_sqd;
                   if (nnl.isCapacityReached()) {
                       max_dist_sqd = nnl.getMaxPriority();
                   }
                   else {
                       max_dist_sqd = Double.MAX_VALUE;
                   }
               }

               // 10.2 Recursively call Nearest Neighbor with parameters
               //      (further-kd, target, further-hr, max-dist_sqd),
               //      storing results in temp-nearest and temp-dist-sqd
               nnbr(further_kd, target, further_hr, max_dist_sqd, lev + 1, K, nnl, checker, timeout);
           }
       }


        // constructor is used only by class; other methods are static
        private KDNode(HPoint key, T val) {

        k = key;
        v = val;
        left = null;
        right = null;
        deleted = false;
        }

        protected String toString(int depth) {
        String s = k + "  " + v + (deleted ? "*" : "");
        if (left != null) {
            s = s + "\n" + pad(depth) + "L " + left.toString(depth+1);
        }
        if (right != null) {
            s = s + "\n" + pad(depth) + "R " + right.toString(depth+1);
        }
        return s;
        }

        private static String pad(int n) {
        String s = "";
        for (int i=0; i<n; ++i) {
            s += " ";
        }
        return s;
        }

        private static void hrcopy(HRect hr_src, HRect hr_dst) {
        hpcopy(hr_src.min, hr_dst.min);
        hpcopy(hr_src.max, hr_dst.max);
        }

        private static void hpcopy(HPoint hp_src, HPoint hp_dst) {
        for (int i=0; i<hp_dst.coord.length; ++i) {
            hp_dst.coord[i] = hp_src.coord[i];
        }
        }
    }

    static class NearestNeighborList<T> {

        static class NeighborEntry<T> implements Comparable<NeighborEntry<T>> {
            final T data;
            final double value;

            public NeighborEntry(final T data,
                                 final double value) {
                this.data = data;
                this.value = value;
            }

            public int compareTo(NeighborEntry<T> t) {
                // note that the positions are reversed!
                return Double.compare(t.value, this.value);
            }
        };

        PriorityQueue<NeighborEntry<T>> m_Queue;
        int m_Capacity = 0;

        // constructor
        public NearestNeighborList(int capacity) {
            m_Capacity = capacity;
            m_Queue = new PriorityQueue<NeighborEntry<T>>(m_Capacity);
        }

        public double getMaxPriority() {
            NeighborEntry p = m_Queue.peek();
            return (p == null) ? Double.POSITIVE_INFINITY : p.value ;
        }

        public boolean insert(T object, double priority) {
            if (isCapacityReached()) {
                if (priority > getMaxPriority()) {
                    // do not insert - all elements in queue have lower priority
                    return false;
                }
                m_Queue.add(new NeighborEntry<T>(object, priority));
                // remove object with highest priority
                m_Queue.poll();
            } else {
                m_Queue.add(new NeighborEntry<T>(object, priority));
            }
            return true;
        }

        public boolean isCapacityReached() {
            return m_Queue.size()>=m_Capacity;
        }

        public T getHighest() {
            NeighborEntry<T> p = m_Queue.peek();
            return (p == null) ?  null : p.data ;
        }

        public boolean isEmpty() {
            return m_Queue.size()==0;
        }

        public int getSize() {
            return m_Queue.size();
        }

        public T removeHighest() {
            // remove object with highest priority
            NeighborEntry<T> p = m_Queue.poll();
            return (p == null) ?  null : p.data ;
        }
    }

    /**
    * KDTree.KeySizeException is thrown when a KDTree method is invoked on a
    * key whose size (array length) mismatches the one used in the that
    * KDTree's constructor.
    *
    * Copyright (C) Simon D. Levy 2014
    *
    * This code is free software: you can redistribute it and/or modify
    * it under the terms of the GNU Lesser General Public License as
    * published by the Free Software Foundation, either version 3 of the
    * License, or (at your option) any later version.
    *
    * This code is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    *  You should have received a copy of the GNU Lesser General Public License
    *  along with this code.  If not, see <http:*www.gnu.org/licenses/>.
    *  You should also have received a copy of the Parrot Parrot AR.Drone
    *  Development License and Parrot AR.Drone copyright notice and disclaimer
    *  and If not, see
    *   <https:*projects.ardrone.org/attachments/277/ParrotLicense.txt>
    * and
    *   <https:*projects.ardrone.org/attachments/278/ParrotCopyrightAndDisclaimer.txt>.
    *
    */
    public static class KeySizeException extends KDException {

        protected KeySizeException() {
        super("Key size mismatch");
        }

        // arbitrary; every serializable class has to have one of these
        public static final long serialVersionUID = 2L;

    }

    public static class KDException extends Exception {
        protected KDException(String s) {
            super(s);
        }
        public static final long serialVersionUID = 1L;
    }

    static class EuclideanDistance extends DistanceMetric {

        protected double distance(double [] a, double [] b)  {

        return Math.sqrt(sqrdist(a, b));

        }

        protected static double sqrdist(double [] a, double [] b) {

        double dist = 0;

        for (int i=0; i<a.length; ++i) {
            double diff = (a[i] - b[i]);
            dist += diff*diff;
        }

        return dist;
        }
    }

    public static class KeyMissingException extends KDException {  /* made public by MSL */

        public KeyMissingException() {
        super("Key not found");
        }

        // arbitrary; every serializable class has to have one of these
        public static final long serialVersionUID = 3L;

    }

    static class HRect implements Serializable{

        protected HPoint min;
        protected HPoint max;

        protected HRect(int ndims) {
        min = new HPoint(ndims);
        max = new HPoint(ndims);
        }

        protected HRect(HPoint vmin, HPoint vmax) {

        min = (HPoint)vmin.clone();
        max = (HPoint)vmax.clone();
        }

        protected Object clone() {

        return new HRect(min, max);
        }

        // from Moore's eqn. 6.6
        protected HPoint closest(HPoint t) {

        HPoint p = new HPoint(t.coord.length);

        for (int i=0; i<t.coord.length; ++i) {
               if (t.coord[i]<=min.coord[i]) {
                   p.coord[i] = min.coord[i];
               }
               else if (t.coord[i]>=max.coord[i]) {
                   p.coord[i] = max.coord[i];
               }
               else {
                   p.coord[i] = t.coord[i];
               }
        }

        return p;
        }

        // used in initial conditions of KDTree.nearest()
        protected static HRect infiniteHRect(int d) {

        HPoint vmin = new HPoint(d);
        HPoint vmax = new HPoint(d);

        for (int i=0; i<d; ++i) {
            vmin.coord[i] = Double.NEGATIVE_INFINITY;
            vmax.coord[i] = Double.POSITIVE_INFINITY;
        }

        return new HRect(vmin, vmax);
        }

        // currently unused
        protected HRect intersection(HRect r) {

        HPoint newmin = new HPoint(min.coord.length);
        HPoint newmax = new HPoint(min.coord.length);

        for (int i=0; i<min.coord.length; ++i) {
            newmin.coord[i] = Math.max(min.coord[i], r.min.coord[i]);
            newmax.coord[i] = Math.min(max.coord[i], r.max.coord[i]);
            if (newmin.coord[i] >= newmax.coord[i]) return null;
        }

        return new HRect(newmin, newmax);
        }

        // currently unused
        protected double area () {

        double a = 1;

        for (int i=0; i<min.coord.length; ++i) {
            a *= (max.coord[i] - min.coord[i]);
        }

        return a;
        }

        public String toString() {
        return min + "\n" + max + "\n";
        }
    }

    static class HammingDistance extends DistanceMetric {

        protected double distance(double [] a, double [] b)  {

        double dist = 0;

        for (int i=0; i<a.length; ++i) {
            double diff = (a[i] - b[i]);
            dist += Math.abs(diff);
        }

        return dist;
        }
    }

    public static interface Editor<T> {
        public T edit(T current) throws KeyDuplicateException;

        public static abstract class BaseEditor<T> implements Editor<T> {
            final T val;
            public BaseEditor(T val) {
                this.val = val;
            }
            public abstract T edit(T current) throws KeyDuplicateException;
        }
        public static class Inserter<T> extends BaseEditor<T> {
            public Inserter(T val) {
                super(val);
            }
            public T edit(T current) throws KeyDuplicateException {
                if (current == null) {
                    return this.val;
                }
                throw new KeyDuplicateException();
            }
        }
        public static class OptionalInserter<T> extends BaseEditor<T> {
            public OptionalInserter(T val) {
                super(val);
            }
            public T edit(T current) {
                return (current == null) ? this.val : current;
            }
        }
        public static class Replacer<T> extends BaseEditor<T> {
            public Replacer(T val) {
                super(val);
            }
            public T edit(T current) {
                return this.val;
            }
        }
    }

    static class HPoint implements Serializable{

        protected double [] coord;

        protected HPoint(int n) {
        coord = new double [n];
        }

        protected HPoint(double [] x) {

        coord = new double[x.length];
        for (int i=0; i<x.length; ++i) coord[i] = x[i];
        }

        protected Object clone() {

        return new HPoint(coord);
        }

        protected boolean equals(HPoint p) {

        // seems faster than java.util.Arrays.equals(), which is not
        // currently supported by Matlab anyway
        for (int i=0; i<coord.length; ++i)
            if (coord[i] != p.coord[i])
            return false;

        return true;
        }

        protected static double sqrdist(HPoint x, HPoint y) {

        return EuclideanDistance.sqrdist(x.coord, y.coord);
        }



        public String toString() {
        String s = "";
        for (int i=0; i<coord.length; ++i) {
            s = s + coord[i] + " ";
        }
        return s;
        }

    }

    public static interface Checker<T> {
        public boolean usable(T v);
    }
}

