import model.*;

import java.util.*;

public final class MyStrategy implements Strategy {

    public static HashMap<Integer, HashMap<Long, Action>> actionsPredictions = new HashMap<>();
    public static HashMap<Integer, HashMap<Long, State>> statesPredictions = new HashMap<>();
    public static Game game;
    public static int debug = 0;
    public static boolean overtime=false;

    @Override
    public void move(Hockeyist self, World world, Game game, Move move){
        init(game, world);
        int currentTick = world.getTick() % Variables.moveCooldown;
        if (currentTick == 0 || puckChangeState(world, self)) {
            Hockeyist[] hockeyists = world.getHockeyists();
            State[][] hockeyistsStates = getHockeyistStates(hockeyists);
            State selfState = hockeyistsStates[0][self.getTeammateIndex()];
            PuckState puckState = getPuckState(world.getPuck(), hockeyistsStates);

            WorldState localWorld = new WorldState(world, hockeyistsStates[0], hockeyistsStates[1], hockeyistsStates[2], puckState, statesPredictions);
            List<Action> actions = new ArrayList<>();
            List<State> states = new ArrayList<>();
            localWorld.printState();
            int tickDiff = currentTick > 0 ? currentTick : Variables.moveCooldown;
            localWorld.getBestAction(selfState, actions, states, 1, 0, Variables.treeMaxDeep, 0, tickDiff);
            setActionsPredictions(self, world, actions, states);

            System.out.println(String.format("%s %s Best actions %s", world.getTick(), self.getId(), actions));
//            localWorld.makeActions(actions[0]).printState();
        }

        setMove(move, getPredictedAction(world.getTick(), self));
        System.out.println(move);

    }

    public boolean puckChangeState(World world, Hockeyist self) {
        Puck puck = world.getPuck();
        if(self.getRemainingCooldownTicks() <= 0 && self.getRemainingKnockdownTicks() <= 0 && puck.getOwnerHockeyistId() != self.getId() &&
                self.getDistanceTo(puck) <= game.getStickLength() && Math.abs(self.getAngleTo(puck)) <= game.getStickSector()){
            return true;
        }
        return false;
    }

    public Action getPredictedAction(int tick, Hockeyist self){
        int key = ((int) tick / Variables.moveCooldown) * Variables.moveCooldown;
        if (actionsPredictions.containsKey(key)) {
            if (actionsPredictions.get(key).containsKey(self.getId())) {
                return actionsPredictions.get(key).get(self.getId());
            }
        }
        return null;
    }

    public PuckState getPuckState(Puck puck, State[][] hockeyistsStates) {
        long ownerHockeyistId = puck.getOwnerHockeyistId();
        State owner = null;
        if (ownerHockeyistId >= 0) {
            for (State s: hockeyistsStates[0]){
                if (s.parent.getId() == ownerHockeyistId) {
                    owner = s;
                }
            }
            for (State s: hockeyistsStates[1]){
                if (s.parent.getId() == ownerHockeyistId) {
                    owner = s;
                }
            }
        }
        return new PuckState(puck, owner);
    }

    public void setActionsPredictions(Hockeyist self, World world, List<Action> actions, List<State> states){
        for(int i = 0; i < actions.size(); i+=1){
            if (actions.get(i) != null) {
                int nextTickStep = i * Variables.moveCooldown;
                int nextTick = world.getTick() + nextTickStep - world.getTick()%Variables.moveCooldown;
                if(!actionsPredictions.containsKey(nextTick)){
                    actionsPredictions.put(nextTick, new HashMap<Long, Action>());
                }
                actionsPredictions.get(nextTick).put(self.getId(), actions.get(i));
            }
        }
        for(int i = 0; i < states.size(); i+=1){
            if (states.get(i) != null) {
                int nextTickStep = i * Variables.moveCooldown;
                int nextTick = world.getTick() + nextTickStep - world.getTick()%Variables.moveCooldown;
                if(!statesPredictions.containsKey(nextTick)){
                    statesPredictions.put(nextTick, new HashMap<Long, State>());
                }
                statesPredictions.get(nextTick).put(self.getId(), states.get(i));
            }
        }

    }

    public void clearCaches(){
        actionsPredictions = new HashMap<>();
        statesPredictions = new HashMap<>();
        Utility.game = null;
    }

    public State[][] getHockeyistStates(Hockeyist[] hockeyists) {
        State[][] hockeyistsStates = new State[3][];
        
        int teammatesCount = 0;
        int enemiesCount = 0;
        int goaliesCount = 0;
        for (int i = 0; i < hockeyists.length; i++) {
            Hockeyist current = hockeyists[i];
            if (current.isTeammate() && current.getType() != HockeyistType.GOALIE){
                teammatesCount++;
            }
            if (!current.isTeammate() && current.getType() != HockeyistType.GOALIE){
                enemiesCount++;
            }
            if (current.getType() == HockeyistType.GOALIE){
                goaliesCount++;
            }
        }
        hockeyistsStates[0] = new State[teammatesCount];
        hockeyistsStates[1] = new State[enemiesCount];
        hockeyistsStates[2] = new State[goaliesCount];
        
        for (int i = 0; i < hockeyists.length; i++) {
            Hockeyist current = hockeyists[i];
            if (current.isTeammate()){
                if(current.getType() != HockeyistType.GOALIE){
                    hockeyistsStates[0][current.getTeammateIndex()] = new State(current);
                }else{
                    hockeyistsStates[2][0] = new State(current);
                }
            }else{
                if(current.getType() != HockeyistType.GOALIE){
                    hockeyistsStates[1][current.getTeammateIndex()] = new State(current);
                }else{
                    hockeyistsStates[2][1] = new State(current);
                }
            }
        }
        return hockeyistsStates;
    }

    public void setMove(Move move, Action bestAction) {
        if (bestAction != null) {
            move.setAction(bestAction.action);
            move.setPassAngle(bestAction.passAngle);
            move.setPassPower(bestAction.passPower);
            move.setSpeedUp(bestAction.speedUp);
            move.setTurn(bestAction.turn);
        }
    }

    public void init(Game game, World world) {
        State.game = game;
        WorldState.game = game;
        Physics.game = game;
        this.game = game;
        PuckState.game = game;
        Utility.init(game, world);
        overtime = Physics.isOvertime(world);
    }

//    public void addCacheAction(long id, Action[] actions) {
//        actionsCache.put(id, new ActionCache(actions, Variables.moveCooldown));
//    }

//    public Action[] checkCache(Hockeyist self) {
//        ActionCache cachedAction = actionsCache.get(self.getId());
//        if (cachedAction != null && cachedAction.cooldown > 0) {
//            cachedAction.cooldown -= 1;
//            return cachedAction.actions;
//        }
//        return null;
//    }
}

