import model.Game;
import model.HockeyistType;
import model.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldState {
    public KDTree<State> kdtree;
    public State[] teammates;
    public State[] enemies;
    public State[] goalies;
    public PuckState puck;
    public int tick;
    public static World world;
    public static Physics physics = new Physics();
    public static Game game;
    public double utility;
    public static HashMap<Integer, HashMap<Long, State>> statesPredictions;

    public WorldState(World world, State[] teammates, State[] enemies, State[] goalies, PuckState puck, HashMap<Integer, HashMap<Long, State>> statesPredictions){
        this.statesPredictions = statesPredictions;
        this.tick = world.getTick();
        WorldState.world = world;
        this.teammates = teammates;
        this.enemies = enemies;
        this.goalies = goalies;
        this.puck = puck;
        this.kdtree = new KDTree<State>(2);
        addToTree(teammates);
        addToTree(enemies);
        addToTree(goalies);
    }

    public void addToTree(State[] states){
        for( State r:states ) {
            if( r!=null ) {
                try {
                    this.kdtree.insert(new double[]{r.x, r.y}, r);
                } catch (KDTree.KeySizeException e) {
                    e.printStackTrace();
                } catch (KDTree.KeyDuplicateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public WorldState(int tick, State[] teammates, State[] enemies, State[] goalies, PuckState puck) {
        this.tick = tick;
        this.teammates = teammates;
        this.enemies = enemies;
        this.goalies = goalies;
        this.puck = puck;
        this.kdtree = new KDTree<State>(2);
        addToTree(teammates);
        addToTree(enemies);
        addToTree(goalies);
    }

    public double getBestAction(State selfState, List<Action> actions, List<State> states, int branchFactor, int deep, int maxDeep, int tickSumm, int tickDiff){
        Action[] availableActions = physics.getAvailableActions(this, selfState);

        double best_utility = -Double.MAX_VALUE;
        List<Action> bestActions = null;
        List<State> bestStates = null;
        for(Action action: availableActions) {
            WorldState nextWorldState = makeActions(action, tickDiff);
            State next_teammate = nextWorldState.teammates[selfState.parent.getTeammateIndex()];

            List<Action> nextActions = new ArrayList<>();
            nextActions.add(action);
            List<State> nextStates = new ArrayList<>();
            nextStates.add(next_teammate);

            if (deep < maxDeep && branchFactor < Variables.maxBranches) {
                this.utility = nextWorldState.getBestAction(next_teammate, nextActions, nextStates, branchFactor*availableActions.length,
                        deep + 1, maxDeep, tickSumm+Variables.moveCooldown, Variables.moveCooldown);
            } else {
                this.utility = nextWorldState.getWorldEffect()/ (tickSumm+tickDiff);
            }

            if (this.utility > best_utility ) {
                best_utility = this.utility;
                bestActions = nextActions;
                bestStates = nextStates;
            }

            if (MyStrategy.debug > 0) {
                this.printState();
                System.out.println(action);
                nextWorldState.printState();
            }
        }
        if (bestActions != null && bestStates != null){
            actions.addAll(bestActions);
            states.addAll(bestStates);
        }

        return best_utility;
    }

    public Action[] getCurrentActions(int[] indexes, Action[][] actions){
        Action[] result = new Action[indexes.length];
        for (int i=0; i < indexes.length; i++) {
            result[i] = actions[i][indexes[i]];
        }
        return result;
    }

    public void iterateIndexes(int[] indexes, Action[][] actions, int position){
        if (indexes[position] >= actions[position].length-1){
            if (position < indexes.length - 1) {
                indexes[position] = 0;
                iterateIndexes(indexes, actions, position + 1);
            } else {
                indexes[position] += 1;
            }
        }else{
            indexes[position] += 1;
        }
    }

    public boolean hasIteration(int[] indexes, Action[][] actions) {
        for (int i=0; i<indexes.length; i++){
            if (indexes[i] >= actions[i].length){
                return false;
            }
        }
        return true;
    }

    public double getWorldEffect() {
        return Utility.getUtility(this);
    }

    public WorldState makeActions(Action action, int tickDiff){
        State[] nextWorldTeammates = new State[this.teammates.length];
        State[] nextWorldEnemies = new State[this.enemies.length];
        State[] nextWorldGoalies = new State[this.goalies.length];

        State puckOwner = null;
        for(State teammate:this.teammates){
            int teammateIndex = teammate.parent.getTeammateIndex();
            if(teammate.getId() == action.owner.getId()){
                nextWorldTeammates[teammateIndex] = this.teammates[teammateIndex].predictState(action, this, tickDiff);
            }else{
                if(statesPredictions.containsKey(this.tick + tickDiff) && statesPredictions.get(this.tick + tickDiff).containsKey(teammate.parent.getId())){
                    nextWorldTeammates[teammateIndex] = statesPredictions.get(this.tick + tickDiff).get(teammate.parent.getId()).copy();
                }else{
                    nextWorldTeammates[teammateIndex] = teammate.copy();
                }
            }

        }

        for(State enemy:this.enemies){
            int enemyIndex = enemy.parent.getTeammateIndex();
            nextWorldEnemies[enemyIndex] = this.enemies[enemyIndex].predictEnemyState(action, this, tickDiff);
        }

        for(State goaly:this.goalies){
            if(goaly.parent.isTeammate()){
                nextWorldGoalies[0] = this.goalies[0].copy();
            }else{
                nextWorldGoalies[1] = this.goalies[1].copy();
            }
        }


        PuckState nextWorldPuck = this.puck.predictState(action, tickDiff, nextWorldTeammates, nextWorldEnemies);
        return new WorldState(this.tick + tickDiff, nextWorldTeammates, nextWorldEnemies, nextWorldGoalies, nextWorldPuck);
    }

    public State getNextHockeyist() {
        State nextHockeyist = null;
        int minMoveCooldown = 9999;
        for (State state: teammates) {
            if (state.parent.isTeammate() && state.parent.getType() != HockeyistType.GOALIE && state.moveCooldown < minMoveCooldown) {
                nextHockeyist = state;
                minMoveCooldown = state.moveCooldown;
            }
        }
        return nextHockeyist;
    }

    public void printState() {


        int scale = 30;
        int newX = (int)(game.getRinkRight() + 200) / scale;
        int newY = (int)(game.getRinkBottom() + 200)/ scale;
        String[][] scaleWorld = new String[newX+1][newY+1];
        for(int i = 0; i <= newX; i++){
            for(int j = 0; j <= newY; j++){
                if (i == 0 || j == 0 || i == newX || j == newY){
                    scaleWorld[i][j] = "##";
                }else{
                    scaleWorld[i][j] = "  ";
                }
            }
        }

        for(State state: teammates){
            scaleWorld[(int)(state.x/scale)][(int)(state.y/scale)] = String.format("%2s", state.parent.getId());
        }
        for(State state: enemies){
            scaleWorld[(int)(state.x/scale)][(int)(state.y/scale)] = String.format("%2s", state.parent.getId());
        }
        scaleWorld[(int)puck.x/scale][(int)puck.y/scale] = "PU";

        System.out.println(String.format("============================ Tick: %s ================================", tick));
        for(int j = 0; j <= newY; j++){
            for(int i = 0; i <= newX; i++){
                System.out.print(scaleWorld[i][j]);
            }
            System.out.println();
        }
        for(State state: teammates){
            System.out.println(String.format("%s: %s Distance: %.2s", state.parent.getId(), state, state.getDistance(puck)));
        }
        for(State state: enemies){
            System.out.println(String.format("%s: %s Distance: %.2s", state.parent.getId(), state, state.getDistance(puck)));
        }
        System.out.println(puck);
        System.out.println("======================================================================");

    }
}
