/**
 * Created by overnote on 9/10/14.
 */
class ActionCache {
    public Action[] actions;
    public int cooldown;
    public ActionCache(Action[] actions, int cooldown) {
        this.actions = actions;
        this.cooldown = cooldown;
    }
}
