import model.*;
import org.junit.After;
import org.junit.Before;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.Math;
import java.util.Map;

public class MyStrategyTest{
    private Game game;

    @Before
    public void setUp() throws Exception {
        double paramDouble = 0.0167;
        //com.a.b.a.a.b.e.b
        this.game = new Game(0, 0, 1200.0D, 800.0D, 360.0D, 65.0D, 200.0D, 150.0D, 65.0D, 770.0D, 1135.0D, 300, 2000, 60, 10, 30, 10, 120.0D, 0.5235987755982988D, 2.094395102393195D, 100, 0.05D, 0.95D, 0.03490658503988659D, 0.02617993877991495D, 0.6D, 0.25D, 20, 0.75D, 0.0125D, 0.75D, 0.5D, 40.0D, 60.0D * paramDouble, 60.0D, 0.75D, 2000.0D, 0.5D, 1.0D, 0.75D, 1.0D, 1.0D, 10.0D, 10.0D, 20.0D, 0.5D, 0.0D, 40.0D, 6.0D, 900.0D * paramDouble, 240.0D * paramDouble, 12500.0D * paramDouble * paramDouble / 30.0D, 7500.0D * paramDouble * paramDouble / 30.0D, 0.0523598775598299D, 100, 100, 100, 100, 110, 80, 105, 105, 105, 110, 80, 105, 80, 120, 1200.0D * paramDouble, 55.0D);
    }

    @After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void testMove() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 1;

        Player[] players = {getPlayer(0)};
        Hockeyist[] hockeyists = new Hockeyist[1];
        Hockeyist self = getHockeyist(1, 0, 0, 600, 250, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)(-game.getHockeyistTurnAngleFactor()), (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.getSpeedUp());


        myStrategy.clearCaches();
        hockeyists = new Hockeyist[1];
        self = getHockeyist(1, 0, 0, 600, 350, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        puck = getPuck(300, 350, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)0.0, (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.getSpeedUp());


        myStrategy.clearCaches();
        hockeyists = new Hockeyist[1];
        self = getHockeyist(1, 0, 0, 600, 350, 0, 0, Math.PI/3, true);
        hockeyists[0] = self;
        puck = getPuck(1000, 10, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)(-game.getHockeyistTurnAngleFactor()), (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.getSpeedUp());


        myStrategy.clearCaches();
        hockeyists = new Hockeyist[1];
        self = getHockeyist(1, 0, 0, 400, 250, 0, 0, 0, true);
        hockeyists[0] = self;
        puck = getPuck(500, 350, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)(game.getHockeyistTurnAngleFactor()), (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.getSpeedUp());


        myStrategy.clearCaches();
        hockeyists = new Hockeyist[1];
        self = getHockeyist(1, 0, 0, 400, 450, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        puck = getPuck(500, 350, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)(game.getHockeyistTurnAngleFactor()), (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)0d, (Double)move.getSpeedUp());
    }

    @org.junit.Test
    public void testMoveTwo() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 600, 250, 0, 0, Math.PI, true);
        Hockeyist self2 = getHockeyist(2, 0, 1, 400, 250, 0, 0, Math.PI, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self,self2};
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals((Double)(-game.getHockeyistTurnAngleFactor()), (Double)move.getTurn());
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.getSpeedUp());
    }

    @org.junit.Test
    public void testPuckProbability() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 1;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 600, 250, 0, 0, Math.PI, true);
        Hockeyist self2 = getHockeyist(2, 0, 1, 450, 250, 0, 0, 0, true);
        Hockeyist self3 = getHockeyist(3, 1, 0, 550, 220, 0, 0, 0, false);
        Hockeyist[] hockeyists = new Hockeyist[]{self, self2, self3};
        Puck puck = getPuck(550, 250, 0, 0, 3, 1);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);

        Move move2 = new Move();
        myStrategy.move(self2, world, game, move2);
        System.out.println(move2);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move.action);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move2.action);
    }

    @org.junit.Test
    public void testKnockdown() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 600, 250, 0, 0, Math.PI, true);
        self.remainingKnockdownTicks = 20;
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.NONE, move.action);
    }

    @org.junit.Test
    public void testGoalProbability() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 800, 250, 0, 0, Math.PI, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);

        puck = getPuck(500, 350, 0, 0);
        PuckState puckState = new PuckState(puck, null);
        System.out.println(puckState.goalProbability());
        org.junit.Assert.assertEquals((Double)0.0, (Double)puckState.goalProbability());

        puck = getPuck(300, 300, -11, 11);
        puckState = new PuckState(puck, null);
        System.out.println(puckState.goalProbability());
        org.junit.Assert.assertTrue(puckState.goalProbability() > 0);
    }

    @org.junit.Test
    public void testPassShoot() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 800, 250, 0, 0, Math.PI, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);

        self = getHockeyist(0, 0, 0, 450, 350, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(430, 370, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);

        org.junit.Assert.assertEquals(ActionType.PASS, move.action);
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.passPower);


        self = getHockeyist(0, 0, 0, 150, 150, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(100, 150, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        org.junit.Assert.assertEquals(ActionType.NONE, move.action);
    }

    @org.junit.Test
    public void testPassShootRight() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 300, 250, 0, 0, Math.PI, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);

        self = getHockeyist(0, 0, 0, 850, 350, 0, 0, 0, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(870, 370, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        org.junit.Assert.assertEquals(ActionType.PASS, move.action);
        org.junit.Assert.assertEquals((Double)1.0, (Double)move.passPower);


        self = getHockeyist(0, 0, 0, 1050, 150, 0, 0, -Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(1100, 150, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        org.junit.Assert.assertEquals(ActionType.NONE, move.action);
    }

    @org.junit.Test
    public void testActionCooldown() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;

        Player[] players = {getPlayer(0)};

        Hockeyist self = getHockeyist(1, 0, 0, 600, 250, 0, 0, Math.PI, true);
        self.remainingCooldownTicks = 20;
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(500, 250, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();

        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertTrue(ActionType.TAKE_PUCK != move.action);
    }

    @org.junit.Test
    public void testTakePuck() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;
        Player[] players = {getPlayer(0), getPlayer(1)};


        Hockeyist self = getHockeyist(1, 1, 0, 500, 300, 0, 0, 0, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(600, 320, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move.getAction());


        self = getHockeyist(1, 1, 0, 500, 300, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(550, 320, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertTrue(ActionType.NONE == move.action);


        myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        self = getHockeyist(1, 1, 0, 500, 300, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(460, 300, 0, 0, self.getId(), players[0].getId());
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertTrue(move.getAction() != ActionType.TAKE_PUCK);


        myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        self = getHockeyist(0, 0, 0, 500, 300, 0, 0, 0, true);
        Hockeyist enemy = getHockeyist(1, 1, 0, 600, 300, 0, 0, Math.PI, false);
        hockeyists = new Hockeyist[]{self, enemy};
        puck = getPuck(550, 300, 0, 0, enemy.getId(), players[1].getId());
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move.getAction());
    }

    @org.junit.Test
    public void testTakePuck2() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 1;
        Player[] players = {getPlayer(0), getPlayer(1)};

        Hockeyist self = getHockeyist(0, 0, 0, 577, 443, -1.15, -0.05, -2.1, true);
        Hockeyist self2 = getHockeyist(1, 0, 1, 641, 442, -0.76, -0.08, -2.7, true);
        Hockeyist enemy = getHockeyist(2, 1, 0, 517, 446, 2.39, 0, 0.2, false);
        Hockeyist enemy2 = getHockeyist(3, 1, 1, 451, 446, 2.99, 0, -0.02, false);
        Hockeyist[] hockeyists = new Hockeyist[]{self,self2,enemy,enemy2};
        Puck puck = getPuck(571, 458, 0, 0, 2, 1);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
//        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move.getAction());

    }

    @org.junit.Test
    public void testTakePuck3() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 3;
        Player[] players = {getPlayer(0), getPlayer(1)};

        Hockeyist self = getHockeyist(0, 0, 0, 326, 200, -3.2, 0.0, 3.0, true);
        Hockeyist self2 = getHockeyist(1, 0, 1, 237, 200, -3.5, 0.0, 0.0, true);
        Hockeyist enemy = getHockeyist(2, 1, 0, 564, 184, -2.9, 0.4, 3, false);
        Hockeyist enemy2 = getHockeyist(3, 1, 1, 520, 228, -4.1, -0.4, -3, false);
        Hockeyist[] hockeyists = new Hockeyist[]{self,self2,enemy,enemy2};
        Puck puck = getPuck(271, 203, -3.1, 0.0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move.getAction());

        Move move2 = new Move();
        myStrategy.move(self2, world, game, move2);
        System.out.println(move2);
        org.junit.Assert.assertEquals(ActionType.TAKE_PUCK, move2.getAction());

    }

    @org.junit.Test
    public void testCache() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 0;
        Player[] players = {getPlayer(0)};


        Hockeyist self = getHockeyist(1, 0, 0, 600, 320, 0, 0, Math.PI, true);
        Hockeyist[] hockeyists = new Hockeyist[]{self};
        Puck puck = getPuck(400, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move1 = new Move();
        myStrategy.move(self, world, game, move1);
        self = getHockeyist(1, 1, 0, 600, 380, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(400, 350, 0, 0);
        World world2 = getWorld(5, players, hockeyists, puck);
        Move move2 = new Move();
        myStrategy.move(self, world2, game, move2);
        org.junit.Assert.assertEquals((Double)move2.getTurn(), (Double)move1.getTurn());
        org.junit.Assert.assertEquals((Double)move2.getSpeedUp(), (Double)move1.getSpeedUp());


        self = getHockeyist(1, 1, 0, 400, 380, 0, 0, Math.PI, true);
        hockeyists = new Hockeyist[]{self};
        puck = getPuck(500, 350, 0, 0);
        World world3 = getWorld(Variables.moveCooldown, players, hockeyists, puck);
        Move move3 = new Move();
        myStrategy.move(self, world3, game, move3);
        org.junit.Assert.assertTrue(move3.getTurn() != move2.getTurn());
        org.junit.Assert.assertTrue(move3.getSpeedUp() != move2.getSpeedUp());
    }

    @org.junit.Test
    public void testCollisions() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 1;
        Variables.treeMaxDeep = 1;

        Player[] players = {getPlayer(0)};
        Hockeyist[] hockeyists = new Hockeyist[2];
        Hockeyist self = getHockeyist(0, 0, 0, 600, 250, 5, 5, Math.PI, true);
        hockeyists[0] = self;
        Hockeyist self2 = getHockeyist(1, 0, 1, 650, 300, 0, 0, Math.PI, true);
        hockeyists[1] = self2;
        Puck puck = getPuck(500, 350, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        myStrategy.move(self, world, game, new Move());

        State[][] hockeyistsStates = myStrategy.getHockeyistStates(hockeyists);
        State selfState = new State(self);
        PuckState puckState = new PuckState(world.getPuck(), null);
        WorldState localWorld = new WorldState(world, hockeyistsStates[0], hockeyistsStates[1], hockeyistsStates[2], puckState, new HashMap<Integer, HashMap<Long, State>>());
        State predict = selfState.predictState(new Action(selfState, 0.5, 0.01d), localWorld, Variables.moveCooldown);
        System.out.println(predict);
        org.junit.Assert.assertTrue(predict.x - 608 <= 1);
        org.junit.Assert.assertTrue(predict.y - 258 <= 1);
    }

    public Hockeyist getHockeyist(long id, int playerId, int teammateIndex, double x, double y, double speedX, double speedY, double angle, boolean teammate) {
        return new Hockeyist(id, playerId, teammateIndex, 30, 30, x, y, speedX, speedY, angle, 0, teammate, HockeyistType.VERSATILE,
                100, 100, 100, 100, 100, HockeyistState.ACTIVE, 1, 0, 0, 0, ActionType.NONE, 0);
    }

    public Puck getPuck (double x, double y, double speedX, double speedY, long hockeyist, long player) {
        return new Puck(1000, 5, 20, x, y, speedX, speedY, hockeyist, player);
    }

    public Puck getPuck (double x, double y, double speedX, double speedY) {
        return new Puck(1000, 5, 20, x, y, speedX, speedY, -1, -1);
    }

    public Player getPlayer(long id) {
        return new Player(id, true, "", 0, false, 0, 0, 0, 0, 0, 0 ,false, false);
    }

    public World getWorld(int tick, Player[] players, Hockeyist[] hockeyists, Puck puck) {
        return new World(tick, 3000, 1024, 768, players, hockeyists, puck);
    }
}
