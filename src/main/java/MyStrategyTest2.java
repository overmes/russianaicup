import model.*;
import org.junit.After;
import org.junit.Before;

import java.util.HashMap;

public class MyStrategyTest2{
    private Game game;

    @Before
    public void setUp() throws Exception {
        double paramDouble = 0.0167;
        //com.a.b.a.a.b.e.b
        this.game = new Game(0, 0, 1200.0D, 800.0D, 360.0D, 65.0D, 200.0D, 150.0D, 65.0D, 770.0D, 1135.0D, 300, 2000, 60, 10, 30, 10, 120.0D, 0.5235987755982988D, 2.094395102393195D, 100, 0.05D, 0.95D, 0.03490658503988659D, 0.02617993877991495D, 0.6D, 0.25D, 20, 0.75D, 0.0125D, 0.75D, 0.5D, 40.0D, 60.0D * paramDouble, 60.0D, 0.75D, 2000.0D, 0.5D, 1.0D, 0.75D, 1.0D, 1.0D, 10.0D, 10.0D, 20.0D, 0.5D, 0.0D, 40.0D, 6.0D, 900.0D * paramDouble, 240.0D * paramDouble, 12500.0D * paramDouble * paramDouble / 30.0D, 7500.0D * paramDouble * paramDouble / 30.0D, 0.0523598775598299D, 100, 100, 100, 100, 110, 80, 105, 105, 105, 110, 80, 105, 80, 120, 1200.0D * paramDouble, 55.0D);
    }

    @org.junit.Test
    public void testTurnForHitPuck() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 2;
        Player[] players = {getPlayer(0)};


        Hockeyist[] hockeyists = new Hockeyist[1];
        Hockeyist self = getHockeyist(0, 0, 0, 200, 250, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        Puck puck = getPuck(500, 350, 0, 0, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);


        hockeyists = new Hockeyist[1];
        self = getHockeyist(0, 0, 0, 850, 450, 0, 0, Math.PI/24, true);
        hockeyists[0] = self;
        puck = getPuck(870, 430, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertTrue(move.turn < -0.03 && move.turn > -0.045);

    }

    @org.junit.Test
    public void testPuckHit() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 1;
        Player[] players = {getPlayer(0)};


        Hockeyist[] hockeyists = new Hockeyist[1];
        Hockeyist self = getHockeyist(0, 0, 0, 200, 250, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        Puck puck = getPuck(500, 350, 0, 0, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);


        hockeyists = new Hockeyist[1];
        self = getHockeyist(0, 0, 0, 750, 550, 0, 0, -0.43, true);
        self.swingTicks = 20;
        hockeyists[0] = self;
        puck = getPuck(770, 530, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.STRIKE, move.action);

    }

    @org.junit.Test
    public void testPuckHitSwing() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 1;
        Player[] players = {getPlayer(0)};


        Hockeyist[] hockeyists = new Hockeyist[1];
        Hockeyist self = getHockeyist(0, 0, 0, 200, 250, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        Puck puck = getPuck(500, 350, 0, 0, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);


        hockeyists = new Hockeyist[1];
        self = getHockeyist(0, 0, 0, 950, 450, 0, 0, -0.39, true);
        self.swingTicks = 20;
        self.state = HockeyistState.SWINGING;
        hockeyists[0] = self;
        puck = getPuck(970, 430, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.STRIKE, move.action);

    }

    @org.junit.Test
    public void testSwing() throws Exception {
        MyStrategy myStrategy = new MyStrategy();
        myStrategy.clearCaches();
        MyStrategy.debug = 0;
        Variables.treeMaxDeep = 2;
        Player[] players = {getPlayer(0)};


        Hockeyist[] hockeyists = new Hockeyist[1];
        Hockeyist self = getHockeyist(0, 0, 0, 200, 250, 0, 0, Math.PI, true);
        hockeyists[0] = self;
        Puck puck = getPuck(500, 350, 0, 0, 0, 0);
        World world = getWorld(0, players, hockeyists, puck);
        Move move = new Move();
        myStrategy.move(self, world, game, move);


        hockeyists = new Hockeyist[1];
        self = getHockeyist(0, 0, 0, 850, 450, 0, 0, -0.276, true);
        self.swingTicks = 0;
        hockeyists[0] = self;
        puck = getPuck(870, 430, 0, 0, 0, 0);
        world = getWorld(0, players, hockeyists, puck);
        move = new Move();
        myStrategy.move(self, world, game, move);
        System.out.println(move);
        org.junit.Assert.assertEquals(ActionType.SWING, move.action);

    }

    public Hockeyist getHockeyist(long id, int playerId, int teammateIndex, double x, double y, double speedX, double speedY, double angle, boolean teammate) {
        return new Hockeyist(id, playerId, teammateIndex, 30, 30, x, y, speedX, speedY, angle, 0, teammate, HockeyistType.VERSATILE,
                100, 100, 100, 100, 100, HockeyistState.ACTIVE, 1, 0, 0, 0, ActionType.NONE, 0);
    }

    public Puck getPuck (double x, double y, double speedX, double speedY, long hockeyist, long player) {
        return new Puck(1000, 5, 20, x, y, speedX, speedY, hockeyist, player);
    }

    public Puck getPuck (double x, double y, double speedX, double speedY) {
        return new Puck(1000, 5, 20, x, y, speedX, speedY, -1, -1);
    }

    public Player getPlayer(long id) {
        return new Player(id, true, "", 0, false, 0, 0, 0, 0, 0, 0 ,false, false);
    }

    public World getWorld(int tick, Player[] players, Hockeyist[] hockeyists, Puck puck) {
        return new World(tick, 3000, 1024, 768, players, hockeyists, puck);
    }
}
