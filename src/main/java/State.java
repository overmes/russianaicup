import model.ActionType;
import model.Game;
import model.Hockeyist;
import model.HockeyistState;

import java.util.List;

import static java.lang.StrictMath.PI;

public class State {
    public static Game game;
    public double x;
    public double y;
    public double speedX;
    public double speedY;
    public double angle;
    public Integer lastActionTick;
    public int cooldown=0;
    public int moveCooldown=0;
    public HockeyistState state;
    public int swingTicks=0;
    public int knockdownTicks=0;
    public Hockeyist parent;

    public State(double x, double y){
        this.x = x;
        this.y = y;
    }

    public State(Hockeyist hockeyist) {
        this.x = hockeyist.getX();
        this.y = hockeyist.getY();
        this.speedX = hockeyist.getSpeedX();
        this.speedY = hockeyist.getSpeedY();
        this.angle = hockeyist.getAngle();
        this.lastActionTick = hockeyist.getLastActionTick();
        this.cooldown = hockeyist.getRemainingCooldownTicks();
        this.moveCooldown = 0;
        this.state = hockeyist.getState();
        this.swingTicks = hockeyist.getSwingTicks();
        this.knockdownTicks = hockeyist.getRemainingKnockdownTicks();
        this.parent = hockeyist;
    }

    public State(State hockeyist) {
        this.x = hockeyist.x;
        this.y = hockeyist.y;
        this.speedX = hockeyist.speedX;
        this.speedY = hockeyist.speedY;
        this.angle = hockeyist.angle;
        this.lastActionTick = hockeyist.lastActionTick;
        this.cooldown = hockeyist.cooldown;
        this.moveCooldown = hockeyist.moveCooldown;
        this.state = hockeyist.state;
        this.swingTicks = hockeyist.swingTicks;
        this.knockdownTicks = hockeyist.knockdownTicks;
        this.parent = hockeyist.parent;
    }

    public State(State state, int ticks) {
        this(state);
        if(this.knockdownTicks > 0){
            this.knockdownTicks -= 10;
        }
        if(this.cooldown > 0){
            this.cooldown -= 10;
        }
        if(this.moveCooldown > 0){
            this.moveCooldown -= ticks;
        }
    }

    public State copy(){
        return new State(this);
    }

    public State predictState(Action action, WorldState world, int ticks){
        State predict = new State(this, ticks);

        if (action.action == ActionType.CANCEL_STRIKE) {
            predict.state = HockeyistState.ACTIVE;
        }

        if (action.action == ActionType.SWING){
            predict.swingTicks = predict.swingTicks + ticks > 20 ? 20 : predict.swingTicks + ticks;
            predict.state = HockeyistState.SWINGING;
        }

        if (action.action == ActionType.STRIKE && this.state == HockeyistState.SWINGING){
            predict.state = HockeyistState.ACTIVE;
            predict.swingTicks = 0;
        }

        double a = Math.abs(getA(action.speedUp));
        double radius = this.parent.getRadius();
        double turn = action.turn;
        predict.angle = this.angle + turn *ticks;

        double ax = a * Math.cos(turn*ticks + this.angle);
        double ay = a * Math.sin(turn*ticks + this.angle);
        double x = this.x + this.speedX*ticks + ax * ticks * ticks /2 - a*Math.sin(turn*ticks + this.angle)*turn*ticks*ticks*ticks/6;
        double y = this.y + this.speedY*ticks + ay * ticks * ticks /2 + a*Math.cos(turn*ticks + this.angle)*turn*ticks*ticks*ticks/6;
        predict.x = Physics.normalizeX(x, radius);
        predict.y = Physics.normalizeY(y, radius);
        predict.speedX = this.speedX + (a*Math.cos(this.angle) + ax)*ticks/2;
        predict.speedY = this.speedY + (a*Math.sin(this.angle) + ay)*ticks/2;
        calculateCollision(predict, world);

        predict.moveCooldown = ticks;
        predict.cooldown = getActionCooldown(action.action);
        return predict;
    }

    public int getActionCooldown(ActionType type){
        int res = 0;
        switch (type){
            case NONE: res = 0; break;
            case TAKE_PUCK: res = game.getDefaultActionCooldownTicks(); break;
            case SWING: res = game.getSwingActionCooldownTicks(); break;
            case CANCEL_STRIKE: res = game.getCancelStrikeActionCooldownTicks(); break;
            case STRIKE: res = game.getDefaultActionCooldownTicks(); break;
            case PASS: res = game.getDefaultActionCooldownTicks(); break;
            case SUBSTITUTE: res = game.getDefaultActionCooldownTicks(); break;
        }
        return res;
    }

    public State predictEnemyState(Action action, WorldState world, int ticks) {
        State predict = new State(this);

        double radius = this.parent.getRadius();
        predict.angle = this.angle;

        predict.x = Physics.normalizeX(this.x + this.speedX * ticks, radius);
        predict.y = Physics.normalizeY(this.y + this.speedY * ticks, radius);
        predict.speedX = this.speedX;
        predict.speedY = this.speedY;
        calculateCollision(predict, world);
        return predict;
    }

    public void calculateCollision(State predict, WorldState world){
        double radius = predict.parent.getRadius();
        List<State> collision = null;
        try {
            collision = world.kdtree.range(new double[]{predict.x - radius, predict.y - radius}, new double[]{predict.x + radius, predict.y + radius});
        } catch (KDTree.KeySizeException e) {
        }
        for(State c:collision){
            //TODO: Add collision speed move
            if(c.getId() != getId()){
                double alpha = Physics.getAngle(this, c);
                double distance = Physics.getDistance(this, c);
                predict.x = this.x - (distance-2*radius)*Math.cos(alpha);
                predict.y = this.y - (distance-2*radius)*Math.sin(alpha);
            }
        }
    }

    public double getA(double speedUp) {
        double speedFactor = getSpeedFactor(speedUp);
        return speedFactor*speedUp;
    }

    public double normalizeAngle(double angle) {
        double result = angle;
        if (angle > PI) {
            return angle - 2*PI;
        }
        if (angle < PI) {
            return angle + 2*PI;
        }
        return result;
    }

    public double getSpeedFactor(double speedUp) {
        if (speedUp > 0) {
            return game.getHockeyistSpeedUpFactor();
        } else {
            return game.getHockeyistSpeedDownFactor();
        }
    }

    public double getDistance(PuckState puck) {
        return Physics.getDistance(this, puck);
    }

    public double getDistance(State state) {
        return Physics.getDistance(this, state);
    }

    public long getId(){
        return this.parent.getId();
    }

    public int teammateIndex(){
        return this.parent.getTeammateIndex();
    }

    @Override
    public String toString() {
        return String.format("X: %.0f Y: %.0f Turn: %5.3f SpeedX: %s SpeedY: %5.3f", x, y, angle, speedX, speedY);
    }
}
