import model.ActionType;

public class Action {
    public ActionType action=ActionType.NONE;
    public double passAngle=0.0;
    public double passPower=1.0;
    public double speedUp=0.0;
    public double turn=0.0;
    public State owner;

//    public Action(State owner, double currentSpeedUp, double currentTurn, ActionType actionType) {
//        this.owner = owner;
//        speedUp = currentSpeedUp;
//        turn = currentTurn;
//        action = actionType;
//    }

    public Action(State owner, double currentSpeedUp, double currentTurn) {
        this.owner = owner;
        speedUp = currentSpeedUp;
        turn = currentTurn;
        action = ActionType.NONE;
    }

    public Action(State owner, ActionType actionType) {
        this.owner = owner;
        action = actionType;
    }

    public Action(State self, ActionType actionType, double passAngle, double passPower) {
        this.owner = self;
        this.action = actionType;
        this.passAngle = passAngle;
        this.passPower = passPower;
    }

    public Action(Action movementAction, Action forceAction) {
        owner = movementAction.owner;
        speedUp = movementAction.speedUp;
        turn = movementAction.turn;
        action = forceAction.action;
        passPower = forceAction.passPower;
        passAngle = forceAction.passAngle;
    }

    @Override
    public String toString() {
        return String.format("%s Action: %s Turn: %5.3f SpeedUp: %s PassAngle: %5.3f PassPower: %s ", owner.parent.getId(),action, turn, speedUp, passAngle, passPower);
    }
}
