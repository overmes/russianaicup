import model.Game;
import model.Hockeyist;
import model.HockeyistType;
import model.World;

public class Utility {
    public static Game game;
    public static boolean enemyLeft;
    public static boolean enemyRight;
    public static State enemyTopCorner;
    public static State enemyBottomCorner;
    public static State topShootPoint;
    public static State bottompShootPoint;

    public static double getUtility(WorldState world){
        PuckState puck = world.puck;
        double result = 0.0;

        result += 50*puck.goalProbability();
        boolean isMyPuck = false;
        if (puck.owner != null && puck.owner.parent.isTeammate()) {
            isMyPuck = true;
            result += 5;
        }

        if (isMyPuck){
            for (State state: world.teammates) {
                State target = topShootPoint;
                if (state.y > 400) target = bottompShootPoint;
                double distanceToShootPoint = state.getDistance(target);
                result += (1280 - distanceToShootPoint) / 1280;
            }
        } else {
            for (State state: world.teammates) {
                double distanceToPuck = state.getDistance(puck);
                result += (1280 - distanceToPuck) / 1280;
            }
        }

        double enemiesDistance = 0.0;
        for (State state: world.enemies) {
            double distanceToPuck = state.getDistance(puck);
            enemiesDistance += (1280 - distanceToPuck) / 1280;
        }
        result -= world.enemies.length > 0 ? 0.5*enemiesDistance/world.enemies.length: 0;

        return result;
    }

    public static void init(Game game, World world){
        if (Utility.game == null){
            Utility.game = game;
            for(Hockeyist hockeyist:world.getHockeyists()){
                if(hockeyist.isTeammate()){
                    if(hockeyist.getX() > 600) {
                        enemyLeft = true;
                        enemyRight = false;
                    } else {
                        enemyLeft = false;
                        enemyRight = true;
                    }
                }
            }

            if(enemyLeft){
                enemyTopCorner = new State(game.getRinkLeft() - game.getGoalNetWidth()/2, game.getGoalNetTop());
                enemyBottomCorner = new State(game.getRinkLeft() - game.getGoalNetWidth()/2, game.getGoalNetTop() + game.getGoalNetHeight());
                topShootPoint = new State(enemyTopCorner.x + 400, enemyTopCorner.y - 50);
                bottompShootPoint = new State(enemyBottomCorner.x + 400, enemyBottomCorner.y + 50);
            } else {
                enemyTopCorner = new State(game.getRinkRight() + game.getGoalNetWidth()/2, game.getGoalNetTop());
                enemyBottomCorner = new State(game.getRinkRight() + game.getGoalNetWidth()/2, game.getGoalNetTop() + game.getGoalNetHeight());
                topShootPoint = new State(enemyTopCorner.x - 400, enemyTopCorner.y - 100);
                bottompShootPoint = new State(enemyBottomCorner.x - 400, enemyBottomCorner.y + 100);
            }
        }
    }
}
