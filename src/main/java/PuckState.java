import model.ActionType;
import model.Game;
import model.Puck;

public class PuckState {
    public double probability;
    public double x;
    public double y;
    public double speedX;
    public double angle;
    public double speedY;
    public State owner = null;
    public static Puck parent;
    public static Game game;

    public PuckState(Puck puck, State owner) {
        this.x = puck.getX();
        this.y = puck.getY();
        this.angle = puck.getAngle();
        this.speedX = puck.getSpeedX();
        this.speedY = puck.getSpeedY();
        this.owner = owner;
        this.probability = 100;
        parent = puck;
    }

    public PuckState(PuckState puckState, State owner) {
        this.x = puckState.x;
        this.y = puckState.y;
        this.angle = puckState.angle;
        this.speedX = puckState.speedX;
        this.speedY = puckState.speedY;
        this.owner = owner;
        this.probability = puckState.probability;
    }

    public PuckState predictState(Action action, int ticks, State[] teammates, State[] enemies) {
        //TODO: owner speedX speedY
        //TODO: трение
        PuckState predict = new PuckState(this, owner);
        double fullSpeed = Math.abs(predict.speedX) + Math.abs(predict.speedY);
        if (owner == null) {
            if (action.action == ActionType.TAKE_PUCK) {
                predict.owner = action.owner;
                predict.probability = game.getPickUpPuckBaseChance() - fullSpeed*5;
            } else {
                predictPuckMove(predict, ticks);
            }
        } else {
            if (action.action == ActionType.NONE) {
                setPuckOwnerCoords(predict, teammates, enemies);
            }
            if (action.action == ActionType.TAKE_PUCK) {
                predict.owner = action.owner;
                predict.probability = game.getTakePuckAwayBaseChance();
                setPuckOwnerCoords(predict, teammates, enemies);
            }
            if (action.action == ActionType.PASS) {
                predict.owner = null;
                predict.speedX = 15*Math.cos(action.passAngle+owner.angle)*action.passPower;
                predict.speedY = 15*Math.sin(action.passAngle + owner.angle)*action.passPower;
                predictPuckMove(predict, ticks);
            }


        }

        if (action.action == ActionType.STRIKE && Physics.isInStickSector(action.owner, this)) {
            double strength = (action.owner.swingTicks*0.0125 + 0.75)*20;
            predict.speedX = strength*Math.cos(owner.angle);
            predict.speedY = strength*Math.sin(owner.angle);
            predictPuckMove(predict, ticks);
            if (owner.getId() == action.owner.getId()){
                predict.owner = null;
            } else {
                predict.probability = 0.75;
            }
        }

        return predict;
    }

    public void predictPuckMove(PuckState predict, int ticks){
        double fullSpeed = Math.abs(predict.speedX) + Math.abs(predict.speedY);
        double radius = PuckState.parent.getRadius();
        double xdiff = 0, ydiff = 0;
        if(fullSpeed > 1){
            double cooef = 0.017;
            xdiff = (predict.speedX/fullSpeed) * cooef *ticks*ticks/2;
            ydiff = (predict.speedY/fullSpeed) * cooef *ticks*ticks/2;
        }
        predict.x = predict.x + predict.speedX*ticks - xdiff;
        predict.y = Physics.normalizeY(predict.y + predict.speedY*ticks - ydiff, radius);
    }

    public void setPuckOwnerCoords(PuckState predict, State[] teammates, State[] enemies){
        State[] owners = teammates;
        if (!owner.parent.isTeammate()) owners = enemies;
        double radius = parent.getRadius();
        State newOwnerState = owners[owner.teammateIndex()];
        predict.x = Physics.normalizeX(newOwnerState.x + Math.cos(newOwnerState.angle)*game.getPuckBindingRange(), radius);
        predict.y = Physics.normalizeY(newOwnerState.y + Math.sin(newOwnerState.angle)*game.getPuckBindingRange(), radius);
        predict.owner = newOwnerState;
    }

    public double goalProbability(){
        double result = 0.0;
        double intersectY;
        if(Utility.enemyLeft){
            intersectY = y + speedY*(game.getRinkLeft()-x)/speedX;
        }else{
            intersectY = y + speedY*(game.getRinkRight()-x)/speedX;
        }
        if(intersectY > game.getGoalNetTop() && intersectY < game.getGoalNetTop()+ game.getGoalNetHeight()){
            if(MyStrategy.overtime){
                result = 1.0;
            }else{
                result = Math.abs(speedY)/6 - 1;
            }
        }

        if(result < 0) result = 0.0;
        if(result > 1) result = 1.0;
        return result;
    }

    @Override
    public String toString() {
        return String.format("X: %.0f, Y: %.0f, SpeedX: %s SpeedY: %5.3f, Owner: %s, ", x, y, speedX, speedY, owner);
    }
}
