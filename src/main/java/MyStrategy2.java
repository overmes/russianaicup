import model.*;

import java.util.Arrays;
import java.util.HashMap;

public final class MyStrategy2 implements Strategy {
    private static final double STRIKE_ANGLE = 1.0D * Math.PI / 180.0D;


    @Override
    public void move(Hockeyist self, World world, Game game, Move move) {
        if (world.getPuck().getOwnerHockeyistId() == self.getId()) {
            Player opponentPlayer= world.getOpponentPlayer();

            double netX = 0.5D * (opponentPlayer.getNetBack() + opponentPlayer.getNetFront());
            double netY = 0.5D * (opponentPlayer.getNetBottom() + opponentPlayer.getNetTop());

            double angleToNet = self.getAngleTo(netX, netY);
            move.setTurn(angleToNet);

            if (Math.abs(angleToNet) < STRIKE_ANGLE) {
                move.setAction(ActionType.STRIKE);
            }
        } else {
            Action bestAction = new Action(null, 1.0, self.getAngleTo(world.getPuck()));
            bestAction.action = ActionType.TAKE_PUCK;
            setMove(move, bestAction);

        }

    }

    public void setMove(Move move, Action bestAction) {
        if (bestAction != null) {
            move.setAction(bestAction.action);
            move.setPassAngle(bestAction.passAngle);
            move.setPassPower(bestAction.passPower);
            move.setSpeedUp(bestAction.speedUp);
            move.setTurn(bestAction.turn);
        }
    }

}

